package ru.akon.mongodbexample.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.akon.mongodbexample.model.User;
import ru.akon.mongodbexample.service.UserService;

@RequiredArgsConstructor
@org.springframework.web.bind.annotation.RestController
@RequestMapping("user")
@Api(description = "Контроллер", tags = "user")
@Slf4j
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "Создать пользователя")
    @PostMapping(value = "create")
    public String create(@RequestBody User user) {
        return userService.create(user);
    }

    @ApiOperation(value = "Получение пользователя")
    @GetMapping(value = "read")
    public User read(String id) {
        return userService.read(id);
    }

    @ApiOperation(value = "Обновление пользователя")
    @PostMapping(value = "update")
    public String update(@RequestBody User user) {
        return userService.update(user);
    }

    @ApiOperation(value = "Удаление пользователя")
    @DeleteMapping(value = "delete")
    public void delete(String id) {
        userService.delete(id);
    }
}
