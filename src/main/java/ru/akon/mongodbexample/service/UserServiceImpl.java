package ru.akon.mongodbexample.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.akon.mongodbexample.model.User;
import ru.akon.mongodbexample.repository.UserRepository;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public String create(User user) {
        return userRepository.save(user).getId();
    }

    @Override
    public User read(String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public String update(User user) {
        Optional<User> optional = userRepository.findById(user.getId());
        if (optional.isPresent()) {
            User existsUser = optional.get();
            existsUser.setName(user.getName());
            existsUser.setOld(user.getOld());
            return userRepository.save(existsUser).getId();
        } else {
            return userRepository.save(user).getId();
        }
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }
}
