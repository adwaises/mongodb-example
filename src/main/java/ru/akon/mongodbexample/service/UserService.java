package ru.akon.mongodbexample.service;

import ru.akon.mongodbexample.model.User;

public interface UserService {
    String create(User user);
    User read(String id);
    String update(User user);
    void delete(String id);
}
