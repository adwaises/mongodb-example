package ru.akon.mongodbexample.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "ru.akon.mongodbexample.repository")
@RequiredArgsConstructor
public class MongoConfig {

    private final Environment env;

    @Bean
    public MongoClient mongo() {
        ConnectionString connectionString = new ConnectionString("mongodb://"
                + env.getProperty("mongo.host")
                + ":"
                + env.getProperty("mongo.port")
                + "/"
                + env.getProperty("mongo.database")
        );
        MongoCredential credential = MongoCredential
                .createCredential(env.getProperty("mongo.username"),
                        env.getProperty("mongo.database"),
                        env.getProperty("mongo.password").toCharArray());

        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .credential(credential)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), env.getProperty("mongo.database"));
    }
}