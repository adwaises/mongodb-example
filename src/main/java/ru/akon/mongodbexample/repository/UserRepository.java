package ru.akon.mongodbexample.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.akon.mongodbexample.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}
